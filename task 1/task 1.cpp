﻿#include <stdio.h>
#include <Windows.h>
int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int isRunning = 1;
    while (isRunning){
        double x, y;
        int isBelong;
        printf("Enter x and y: ");
        scanf_s("%lf %lf", &x, &y);
        
        if (((y <= x + 1) && (y >= -x + 1) && (y >= 0)) || ((y <= x + 1) && (y <= 0) && (x <= 0)))
            isBelong = 1;
        else
            isBelong = 0;

        printf("%i\n", isBelong);
        if (x == 100) isRunning = 0;
    }
}