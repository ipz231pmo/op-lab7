﻿#include <stdio.h>
#include <Windows.h>
int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int isRunning = 1;
    while (isRunning){
        int n, m;
        printf("Введіть число M та N: ");
        scanf_s("%d %d", &m, &n);
        if (n == 100) isRunning = 0;
        m%n==0 ? printf("%d є дільником числа %d\n", n, m) : printf("%d не є дільником числа %d\n", n, m);
    }
}