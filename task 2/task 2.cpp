﻿#include <stdio.h>
int main(){
    int isRunning = 1;
    while (isRunning){
        double a, b, c;
        printf("Enter coefs A, B, C: ");
        scanf_s("%lf %lf %lf", &a, &b, &c);
        double d = b * b - 4 * a * c;
        int count = (d > 0) ? 2 : (d == 0) ? 1 : 0;
        printf("Equation has %i roots\n", count);
        if (a == 100) isRunning = 0;
    }
}